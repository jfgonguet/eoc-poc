package com.eoc.bpm.service;

import java.util.HashMap;

import org.kie.api.runtime.process.WorkItem;
import org.kie.api.runtime.process.WorkItemHandler;
import org.kie.api.runtime.process.WorkItemManager;
import org.springframework.stereotype.Component;

@Component("MyTask")
public class JeffTaskWorkItemHandler implements WorkItemHandler {

	@Override
	public void executeWorkItem(WorkItem workItem, WorkItemManager manager) {
	    System.out.println("   >>> Work item being executed " + workItem);
	    
	    String name = (String) workItem.getParameter("MyFirstParam");
	    
        HashMap result = new HashMap();
	        HashMap donneesRetour = new HashMap();
	        donneesRetour.put("infoRetour", name + "-MODIFIE...");
        result.put("Result", donneesRetour);

	    System.out.println("   <<< End of Work item");
	    
    
	    manager.completeWorkItem(workItem.getId(), result);
	}

	@Override
	public void abortWorkItem(WorkItem workItem, WorkItemManager manager) {
		// TODO Auto-generated method stub

	}

}
