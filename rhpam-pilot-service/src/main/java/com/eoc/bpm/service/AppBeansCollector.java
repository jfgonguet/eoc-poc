package com.eoc.bpm.service;

import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableScheduling
@PropertySource("classpath:application.properties")

public class AppBeansCollector {

	private final static Logger LOGGER = Logger.getLogger(AppBeansCollector.class.getName());

    @Value("${com.eoc.rhpam.info}")
    private String rhpamInfo;
    @Value("${com.eoc.pathForTempFolder}")
    private String pathForTempFolder;
    
    static private JeffBean beanJeff = null;
    
	@Bean
	public JeffBean getJeffBean() {
		if ( AppBeansCollector.beanJeff == null ) {

			JeffBean jeff = new JeffBean();

			jeff.setRHpamInfo(this.rhpamInfo);
			LOGGER.info("==> Load JEFF-BEAN - RHPAMinfo=" + jeff.getRHpamInfo());

			jeff.setPathForTempFolder(this.pathForTempFolder);
			LOGGER.info("==> Load JEFF-BEAN - PATH_FOR_TEMP_FOLDER=" + jeff.getPathForTempFolder());

			AppBeansCollector.beanJeff = jeff;
		}
		return AppBeansCollector.beanJeff;
	}

}
