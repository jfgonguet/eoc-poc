package com.eoc.bpm.service;

import java.util.logging.Logger;

public class JeffBean {
	
	private final static Logger LOGGER = Logger.getLogger(JeffBean.class.getName());

	private String rhpamInfo;
	private String pathForTempFolder;

	public String getPathForTempFolder() {
		return this.pathForTempFolder;
	}

	public void setPathForTempFolder(String pathForTempFolder) {
		this.pathForTempFolder = pathForTempFolder;
	}

	public String getRHpamInfo() {
		return rhpamInfo;
	}

	public void setRHpamInfo(String info) {
		this.rhpamInfo = info;
	}

	@Override
	public String toString() {
		return "JeffBean [rhpamInfo=" + rhpamInfo + ", pathForTempFolder=" + pathForTempFolder + "]";
	}

}
