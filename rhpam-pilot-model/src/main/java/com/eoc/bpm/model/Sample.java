package com.eoc.bpm.model;

/**
 * Define the Sample to be taken for each Lab400Request
 */

public class Sample implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private java.lang.Boolean isPrescribableIndividually;
	private java.lang.String MRI;
	private java.lang.String descr;
	private java.lang.String tipo;
	private java.lang.String processingMode;
	private java.lang.Boolean isBacteriology;
	private java.lang.Boolean isTakingEditable;

	public Sample() {
	}

	public java.lang.Boolean getIsPrescribableIndividually() {
		return this.isPrescribableIndividually;
	}

	public void setIsPrescribableIndividually(
			java.lang.Boolean isPrescribableIndividually) {
		this.isPrescribableIndividually = isPrescribableIndividually;
	}

	public java.lang.String getMRI() {
		return this.MRI;
	}

	public void setMRI(java.lang.String MRI) {
		this.MRI = MRI;
	}

	public java.lang.String getDescr() {
		return this.descr;
	}

	public void setDescr(java.lang.String descr) {
		this.descr = descr;
	}

	public java.lang.String getTipo() {
		return this.tipo;
	}

	public void setTipo(java.lang.String tipo) {
		this.tipo = tipo;
	}

	public java.lang.String getProcessingMode() {
		return this.processingMode;
	}

	public void setProcessingMode(java.lang.String processingMode) {
		this.processingMode = processingMode;
	}

	public java.lang.Boolean getIsBacteriology() {
		return this.isBacteriology;
	}

	public void setIsBacteriology(java.lang.Boolean isBacteriology) {
		this.isBacteriology = isBacteriology;
	}

	public java.lang.Boolean getIsTakingEditable() {
		return this.isTakingEditable;
	}

	public void setIsTakingEditable(java.lang.Boolean isTakingEditable) {
		this.isTakingEditable = isTakingEditable;
	}

	@Override
	public String toString() {
		return "Sample [isPrescribableIndividually="
				+ this.isPrescribableIndividually + ", MRI=" + this.MRI
				+ ", descr=" + this.descr + ", tipo=" + this.tipo
				+ ", processingMode=" + this.processingMode
				+ ", isBacteriology=" + this.isBacteriology
				+ ", isTakingEditable=" + this.isTakingEditable + "]";
	}

	public Sample(java.lang.Boolean isPrescribableIndividually,
			java.lang.String MRI, java.lang.String descr,
			java.lang.String tipo, java.lang.String processingMode,
			java.lang.Boolean isBacteriology, java.lang.Boolean isTakingEditable) {
		this.isPrescribableIndividually = isPrescribableIndividually;
		this.MRI = MRI;
		this.descr = descr;
		this.tipo = tipo;
		this.processingMode = processingMode;
		this.isBacteriology = isBacteriology;
		this.isTakingEditable = isTakingEditable;
	}

}