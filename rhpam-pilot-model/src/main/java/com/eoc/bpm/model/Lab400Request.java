package com.eoc.bpm.model;

/**
 * Define the LAB400 request attached to a doctor request
 */

public class Lab400Request implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private java.lang.Integer requestNumber;
	private java.lang.Integer responsibleLab;
	private java.lang.Boolean requestAlreadySent;

	private java.util.List<com.eoc.bpm.model.Sample> listSample;

	public Lab400Request() {
	}
	public Lab400Request(java.lang.Integer requestNumber,
			java.lang.Integer responsibleLab,
			java.lang.Boolean alreadySent,
			java.util.List<com.eoc.bpm.model.Sample> listSample) {
		this.requestNumber = requestNumber;
		this.responsibleLab = responsibleLab;
		this.requestAlreadySent = alreadySent;
		this.listSample = listSample;
	}

	public java.lang.Integer getRequestNumber() {
		return this.requestNumber;
	}
	public void setRequestNumber(java.lang.Integer requestNumber) {
		this.requestNumber = requestNumber;
	}

	public java.lang.Integer getResponsibleLab() {
		return this.responsibleLab;
	}
	public void setResponsibleLab(java.lang.Integer responsibleLab) {
		this.responsibleLab = responsibleLab;
	}

	public java.lang.Boolean getRequestAlreadySent() {
		return requestAlreadySent;
	}
	public void setRequestAlreadySent(java.lang.Boolean requestAlreadySent) {
		this.requestAlreadySent = requestAlreadySent;
	}

	public java.util.List<com.eoc.bpm.model.Sample> getListSample() {
		return this.listSample;
	}

	public void setListSample(
			java.util.List<com.eoc.bpm.model.Sample> listSample) {
		this.listSample = listSample;
	}

	@Override
	public String toString() {
		String result = "Lab400Request [requestNumber=" + this.requestNumber
				+ ", responsibleLab=" + this.responsibleLab + ", listSample=";
		if (getListSample() != null) {
			for (int i = 0; i < getListSample().size(); i++) {
				result = result + getListSample().get(i).toString();
			}
		}
		result = result + "]";
		return result;
	}
}