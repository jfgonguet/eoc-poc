package com.eoc.bpm.model;

/**
 * Define the request posted by the doctor on the PrescoLab application
 */

public class DoctorRequest implements java.io.Serializable {

	static final long serialVersionUID = 1L;

	private java.lang.String requestUUID;
	private java.lang.Boolean daPlanificare;
	private java.lang.String luogoDelPrelievo;
	private java.lang.Boolean richiestaUrgente;
	private java.lang.String medicoPrescrivente;

	private java.util.List<com.eoc.bpm.model.Lab400Request> listLab400Request;

	public DoctorRequest() {
	}

	public java.lang.String getRequestUUID() {
		return this.requestUUID;
	}

	public void setRequestUUID(java.lang.String requestUUID) {
		this.requestUUID = requestUUID;
	}

	public java.lang.Boolean getDaPlanificare() {
		return this.daPlanificare;
	}

	public void setDaPlanificare(java.lang.Boolean daPlanificare) {
		this.daPlanificare = daPlanificare;
	}

	public java.lang.String getLuogoDelPrelievo() {
		return this.luogoDelPrelievo;
	}

	public void setLuogoDelPrelievo(java.lang.String luogoDelPrelievo) {
		this.luogoDelPrelievo = luogoDelPrelievo;
	}

	public java.lang.Boolean getRichiestaUrgente() {
		return this.richiestaUrgente;
	}

	public void setRichiestaUrgente(java.lang.Boolean richiestaUrgente) {
		this.richiestaUrgente = richiestaUrgente;
	}

	public java.lang.String getMedicoPrescrivente() {
		return this.medicoPrescrivente;
	}

	public void setMedicoPrescrivente(java.lang.String medicoPrescrivente) {
		this.medicoPrescrivente = medicoPrescrivente;
	}

	public java.util.List<com.eoc.bpm.model.Lab400Request> getListLab400Request() {
		return this.listLab400Request;
	}

	public void setListLab400Request(
			java.util.List<com.eoc.bpm.model.Lab400Request> listLab400Request) {
		this.listLab400Request = listLab400Request;
	}

	@Override
	public String toString() {
		String result = "DoctorRequest [requestUUID=" + this.requestUUID
				+ ", daPlanificare=" + this.daPlanificare
				+ ", luogoDelPrelievo=" + this.luogoDelPrelievo
				+ ", richiestaUrgente=" + this.richiestaUrgente
				+ ", medicoPrescrivente=" + this.medicoPrescrivente
				+ ", listLab400Request=";
		if (getListLab400Request() != null) {
			for (int i = 0; i < getListLab400Request().size(); i++) {
				result = result + getListLab400Request().get(i).toString();
			}
		}
		result = result + "]";
		return result;
	}

	public DoctorRequest(java.lang.String requestUUID,
			java.lang.Boolean daPlanificare, java.lang.String luogoDelPrelievo,
			java.lang.Boolean richiestaUrgente,
			java.lang.String medicoPrescrivente,
			java.util.List<com.eoc.bpm.model.Lab400Request> listLab400Request) {
		this.requestUUID = requestUUID;
		this.daPlanificare = daPlanificare;
		this.luogoDelPrelievo = luogoDelPrelievo;
		this.richiestaUrgente = richiestaUrgente;
		this.medicoPrescrivente = medicoPrescrivente;
		this.listLab400Request = listLab400Request;
	}
}