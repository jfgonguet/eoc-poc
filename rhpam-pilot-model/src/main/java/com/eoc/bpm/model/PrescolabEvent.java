package com.eoc.bpm.model;

public class PrescolabEvent {
	
	public String eventName;
    private String eventValue;

	public PrescolabEvent() {
		super();
	}

	public PrescolabEvent(String eventName, String eventValue) {
		super();
		this.eventName = eventName;
		this.eventValue = eventValue;
	}

	public String getEventName() {
		return eventName;
	}
	public void setEventName(String eventName) {
		this.eventName = eventName;
	}
	public String getEventValue() {
		return eventValue;
	}
	public void setEventValue(String eventValue) {
		this.eventValue = eventValue;
	}
	
	@Override
	public String toString() {
		return "PrescolabEvent [eventName=" + eventName + ", eventValue=" + eventValue + "]";
	}

}
